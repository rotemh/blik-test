﻿using Blik_Test.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blik_Test
{
    internal static class HelperMethods
    {
        private static readonly Random _rnd = new();

        internal static List<Hobby> GenerateHobbiesList(List<Hobby> hobbies)
        {
            var result = new List<Hobby>();

            int numberOfHobbies = _rnd.Next(2, 4);

            for (int i = 0; i < numberOfHobbies; i++)
            {
                int index = _rnd.Next(0, 5);
                while (result.Contains(hobbies[index]))
                {
                    index = _rnd.Next(0, 5);
                }

                result.Add(hobbies[index]);
            }

            return result;
        }
    }
}
