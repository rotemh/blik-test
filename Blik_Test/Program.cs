﻿
using Blik_Test;
using Blik_Test.BO;
using Blik_Test.Models;

#region Data to work with

var hobbies = new List<Hobby>
{
    new Hobby(1, "Music"),
    new Hobby(2, "Sleeping"),
    new Hobby(3, "Animation"),
    new Hobby(4, "Video Games"),
    new Hobby(5, "Cooking")
};

int id = 1;

var people = new List<Person>
{
    new Person(id++, "Jimmy Lee", Enums.Gender.Female, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Kathy Shure", Enums.Gender.Male, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Ellen Mark", Enums.Gender.Other, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Maya Agda", Enums.Gender.Female, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Jen Hoper", Enums.Gender.Female, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "John Wick", Enums.Gender.Male, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Jonathan Calm", Enums.Gender.Male, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Natasha Bloom'", Enums.Gender.Female, HelperMethods.GenerateHobbiesList(hobbies)),
    new Person(id++, "Noel Bow", Enums.Gender.Male, HelperMethods.GenerateHobbiesList(hobbies))
};

#endregion Data to work with [end]

// Displaying Data:
people.ForEach(p => Console.WriteLine(p));

// TODO list:

// 1. Find all people with the hobby 'Music' and put it into a list
// your code:

// 2. Find all people with the hobby 'Cooking' and put it into a list
// your code:

// 3. Get all people with both hobbies - 'Music' and 'Cooking'
// your code:

// 4. Create a database and create the needed tables to represent the data of people and hobbies
//    note: People have hobbies.
// Please supply the SQL script for creating those tables with data.
// The SQL script should be in an SQL file attached to the project.

// 5. Implement a data access layer (DAL) to retrieve the data from the database.
//    Please use Stored Procedures and add them to the SQL script file.

// 6. Take a look at class Department.
//    According to the code we are not allowed to be in a negative balance.
//    Is the code OK? When you run the code - does it behave accordingly?
//    If there is a problem in the code - how would you fix it?
var dep = new Department(1000);
Parallel.For(0, 10, (i) => dep.DoTransactions());







